<?php
ob_start();
session_start();
if(isset($_SESSION['adminname']))
{
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Hospital</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-georgia.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body >
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo" align="left">
         <h1><a href="adminhome.php"><strong><font size="+6"><var><dfn><kbd><samp><tt>GOVT.TALUK HOSPITAL,SASTHAMCOTTA</tt></samp></kbd></dfn></var></font></strong></a></h1></div>
      <div class="searchform">
        
      </div>
      <div class="clr"></div>
      <div class="menu_nav">
        <ul>
          <li class="active"><a href="adminhome.php"><span>Home Page</span></a></li>
          <li><a href="adminhome.php?menu=dept"><span>Department details</span></a></li>
          <li><a href="adminhome.php?menu=emp"><span>Employee details</span></a></li>
           <li><a href="adminhome.php?menu=adm"><span>Administrators</span></a></li>
           <li><a href="adminhome.php?menu=vfb"><span>View Feedbacks</span></a></li>
           <li><a href="adminhome.php?menu=chng"><span>Change password</span></a></li>
           <li><a href="logout.php"><span>LogOut</span></a></li>
        </ul>
      </div>
      <!--<div class="clr"></div>
   
      <div class="slider">
       <!-- <div id="coin-slider">
         <a href="#"><img src="images/slide1.jpg" width="960" height="360" alt="" /></a> 
         
         <a href="#"><img src="images/slide2.jpg" width="960" height="360" alt="" /></a>
         
          <a href="#"><img src="images/slide3.jpg" width="960" height="360" alt="" /></a>
           </div>-->
        <div class="clr"></div>
      </div>-->
 
   <!--   <div class="clr"></div>
-->
    </div>  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Welcome Administrator </h2>
         
          
          <div class="clr"></div>
          
          <div class="img">
          
                   <?php
$c=$_REQUEST['menu'];
switch($c)
{
	
	case "dept" :  include "department.php";
	break;
	case "vdept" : include "viewdept.php";
	break;
	case "editdept" : include "editdept.php";
	break;
	case "emp" : include "empdeta.php";
	break;
	case "vemp" : include "viewemp.php";
	break;
	case "editemp": include "editemp.php";
	break;
	case "adm" :  include "admi.php";
	break;
	case "vadmi" : include "viewadmi.php";
	break;
	case "editadmi" : include "editadmi.php";
	break;
	case "vfb" : include "viewfeedbacks.php";
	break;
	case "chng": include "chngpswd.php";
	break;
	
}
?>
          
          </div>
          <div class="post_content">
            <p>
           <!-- content  -->
   
            
            </p>
            
          </div>
          <div class="clr"></div>
        </div>
        <div class="article">
         
          
          <div class="clr"></div></div>
          <div class="post_content">
          <!-- Content @ -->
           
          </div>
          <div class="clr"></div>
        </div>
       
      </div>
      <div class="sidebar">
        <div class="gadget">
          <h2 class="star"><span>Sidebar</span> Menu</h2>
          <div class="clr"></div>
          <ul class="sb_menu">
            <li><a href="adminhome.php">Home</a></li>
            <li><a href="adminhome.php?menu=dept">Department details</a></li>
            <li><a href="adminhome.php?menu=vdept">View and edit Department details</a></li>
            <li><a href="adminhome.php?menu=emp">employee details</a></li>
            <li><a href="adminhome.php?menu=vemp">view and edit employee details</a></li>
             <li><a href="adminhome.php?menu=adm"> Administrators details</a></li>
            <li><a href="adminhome.php?menu=vadmi">View and edit Administrators details</a></li>
                 <li><a href="adminhome.php?menu=chng">change password</a></li>
            <li><a href="adminhome.php?menu=vfb">View Feedbacks</a></li>
            <li><a href="logout.php">LogOut</a></li>
        </div>
          </ul>
        <div class="gadget">
          
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image</span> Gallery</h2>
        <a href="#"><img src="images/gal1.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal2.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal3.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal4.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal5.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal6.jpg" width="75" height="75" alt="" class="gal" /></a> </div>
      <div class="col c2">
        <h2><span>Services</span> Overview</h2>
        <p>Curabitur sed urna id nunc pulvinar semper. Nunc sit amet tortor sit amet lacus sagittis posuere cursus vitae nunc.Etiam venenatis, turpis at eleifend porta, nisl nulla bibendum justo.</p>
        <ul class="fbg_ul">
          <li><a href="#">Lorem ipsum dolor labore et dolore.</a></li>
          <li><a href="#">Excepteur officia deserunt.</a></li>
          <li><a href="#">Integer tellus ipsum tempor sed.</a></li>
        </ul>
      </div>
      <div class="col c3">
        <h2><span>Contact</span> Us</h2>
        <p>Nullam quam lorem, tristique non vestibulum nec, consectetur in risus. Aliquam a quam vel leo gravida gravida eu porttitor dui.</p>
        <p class="contact_info"> <span>Address:</span> 1458 TemplateAccess, USA<br />
          <span>Telephone:</span> +123-1234-5678<br />
          <span>FAX:</span> +458-4578<br />
          <span>Others:</span> +301 - 0125 - 01258<br />
          <span>E-mail:</span> <a href="#">mail@yoursitename.com</a> </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; Copyright <a href="#">MyWebSite</a>.</p>
      <p class="rf">Design by Dream Web Templates</a></p>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div align=center></div></body>
</html>
<?php
} else
{
	header("location:index.php");
}
?>
-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2016 at 03:45 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `admi`
--

CREATE TABLE IF NOT EXISTS `admi` (
  `acode` int(15) NOT NULL,
  `scode` varchar(50) NOT NULL,
  `ades` varchar(30) NOT NULL,
  PRIMARY KEY (`acode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admi`
--

INSERT INTO `admi` (`acode`, `scode`, `ades`) VALUES
(6, '18', 'Clerk'),
(5, '17', 'Head Nurse'),
(4, '16', 'Head Nurse'),
(3, '15', 'Head Nurse'),
(1, '1', 'Superiendendent'),
(2, '2', 'RMO');

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `bno` varchar(30) NOT NULL,
  `opno` int(15) NOT NULL,
  `billfor` varchar(30) NOT NULL,
  `age` int(15) NOT NULL,
  `amount` int(15) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bno`, `opno`, `billfor`, `age`, `amount`, `date`) VALUES
('123', 111, 'urin test', 13, 50, '2016-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `cid` int(15) NOT NULL AUTO_INCREMENT,
  `cname` varchar(30) NOT NULL,
  `cmld` varchar(30) NOT NULL,
  `ccno` varchar(30) NOT NULL,
  `cothrfb` text NOT NULL,
  `cdate` date NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `contact`
--


-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dcode` int(50) NOT NULL AUTO_INCREMENT,
  `dname` varchar(50) NOT NULL,
  `dsptn` text NOT NULL,
  PRIMARY KEY (`dcode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23459 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dcode`, `dname`, `dsptn`) VALUES
(1, 'PAEDIATRIC', 'Highly qualified and experienced Doctors in this Department are capable of diagnosing and giving expert management for most of the paediatric conditions.ICU to give special treatment to critically ill children.  Care of children in health and illness is an integral part of any health care delivery system. The advent of the small family norm, has raised parental expectations of the quality of child health care .'),
(2, 'GENERAL SURGARY', 'This department provide high quality personalized patient care.\r\nThis surgery is a surgical specialty that focuses on abdominal contents including esophagus, stomach, small bowel, colon, liver, pancreas, gallbladder and bile ducts, and often the thyroid gland.All surgeons in this hospital are trained in emergency surgery.Bleeding,infections,bowel obstructions and organ perforations are the main problems.\r\n'),
(3, 'GYNAECOLOGY', 'Gynaecology department at taluk hospital is committed to the overall well being of the womens health.\r\nSpecial Features are</br>\r\nâ€¢ Infertility clinic for diagnosing and treatment of couples        \r\n                      who are unable to conceive.</br>\r\nâ€¢ Diagnostic facilities like hysteroscopy (vasascope) to visualize the uterine cavity for infertility, excessive              \r\n       bleeding and amenorrhea. </br>\r\nâ€¢ Facilities for all major gynaecological surgery like cancer of the uterus or ovary and other tumours of the uterus.</br>\r\nâ€¢ Modern operation theatres with a well equipped surgical ICU.</br>\r\nâ€¢ Various treatment modalities are offered for patients with                                                                                                                   excessive bleeding</br> \r\nâ€¢ Medical treatment</br>\r\nâ€¢ Hysterectomy </br>\r\nâ€¢ Endometrial ablation</br>\r\n'),
(4, 'ORTHO', 'The department is headed by an efficient and experienced Orthopaedic Surgeon, who regularly performs surgeries successfully.\r\nDepartment of Orthopaedics provides quality treatment of international standards in a most competent and professional manner for all types of orthopedic problems.\r\n Each doctor in the department is specially trained in all aspects of latest techniques Facilities for free flap grafting, bone grafting, microvascular surgery Experienced consultants are available in 24 hours.'),
(5, 'ANESTHESIA', ' Our board-certified anesthesiologists and certified registered nurse-anesthetists work closely with surgeons,obstetricians and other doctors to provide general, total intravenous and regional anesthesia for surgeries, childbirth and other medical procedures for people of all ages,children through seniors.\r\nThere are three types of anesthesia:</br>\r\n1.Local</br>\r\n2.Regional </br>\r\n3.general anesthesia.'),
(6, 'CASUALITY', 'An Emergency department also known as an accident and emergency department(A&E),emergency room(ER) or casualty department,is a medical treatment facility specializing in emergency medicine,the accurate care of patients who present without prior appointment; by their on means or by that of an ambulance.\r\nThis department must provide initial treatment for a board spectrum of illness and injuries.'),
(7, 'DENTISTRY', 'In dentistry department, we provide a wide range of oral health care services to patients,from routine checkups and cleanings to fitting braces and treating oral disease.'),
(8, 'GENERAL MEDICINE', 'The Department of General Medicine is well manned and is one of the best in North Kerala.\r\n Well trained and very well experienced several senior physicians work as a single team in tackling problem cases and critically ill patients.One of it wings takes care of diabetic clinic which works every working day.\r\nThe Department is equipped with all the requirements for acute medical care and it has access to Lab, Radiology, Nuclear Medicine departments etc.\r\n\r\n'),
(9, 'FACULTY SECTION', 'FACULTY SECTION');

-- --------------------------------------------------------

--
-- Table structure for table `lab`
--

CREATE TABLE IF NOT EXISTS `lab` (
  `tcode` int(30) NOT NULL AUTO_INCREMENT,
  `tcategory` varchar(30) NOT NULL,
  `ttype` varchar(30) NOT NULL,
  `tname` varchar(30) NOT NULL,
  `normval` varchar(50) DEFAULT NULL,
  `aplval` int(11) NOT NULL,
  `bplval` int(11) NOT NULL,
  PRIMARY KEY (`tcode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45436 ;

--
-- Dumping data for table `lab`
--

INSERT INTO `lab` (`tcode`, `tcategory`, `ttype`, `tname`, `normval`, `aplval`, `bplval`) VALUES
(2, 'HAEMATOLOGY', 'BLOOD', 'TC', '4000-1000 cells/cumm', 20, 10),
(3, 'BIOCHEMISTRY', 'LIPID PROFILE', 'RBS', '<40mgm%', 20, 10),
(1, 'HAEMATOLOGY', 'BLOOD', 'HB', 'MEN=13-15gm% WOMEN=12-15gm%', 20, 10),
(4, 'HAEMATOLOGY', 'BLOOD', 'DC', 'Poly=40-70% Lymph=20-50% Eos=2-6%', 20, 10),
(5, 'HAEMATOLOGY', 'BLOOD', 'ESR', 'MEN=3-15gm% WOMEN=5-20gm%', 20, 10),
(6, 'HAEMATOLOGY', 'BLOOD', 'PCV', 'MEN=40-54gm% WOMEN=36-47gm%', 30, 15),
(7, 'HAEMATOLOGY', 'BLOOD', 'BT', '1-6"', 30, 20),
(8, 'HAEMATOLOGY', 'BLOOD', 'CT', '2-6"', 30, 20),
(9, 'HAEMATOLOGY', 'BLOOD', 'GROUP AND RH', '-', 40, 25),
(10, 'HAEMATOLOGY', 'BLOOD', 'AEC', '-', 40, 25),
(11, 'HAEMATOLOGY', 'BLOOD', 'DENGUE', '-', 180, 150),
(12, 'HAEMATOLOGY', 'BLOOD', 'LEPTO', '-', 150, 100),
(13, 'BIOCHEMISTRY', 'LIPID PROFILE', 'CHOLESTEROL', '150-200mgm%', 50, 40),
(14, 'BIOCHEMISTRY', 'WIDAL', 'UREA', '20-40mgm%', 45, 35),
(15, 'HAEMATOLOGY', 'LIPID PROFILE', 'ACETONE AND KETONE BODIES', 'cfgvb ', 40, 25);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `role`) VALUES
('jane', 'jane1234', 'Doctor'),
('surendran', 'surendran1234', 'Clerk'),
('geetha', 'geetha1234', ' Head Nurse'),
('susheela', 'susheela1234', ' Head Nurse'),
('sobha', 'sobha1234', ' Head Nurse'),
('admin', 'admin1234', 'admin'),
('staff', 'staff1234', 'staff'),
('vrinda', 'vrinda1234', 'Doctor'),
('jayadev', 'jayadev1234', 'Doctor'),
('shinu', 'shinu1234', 'Doctor'),
('athira', 'athira1234', 'Doctor'),
('manoj', 'manoj1234', 'Doctor'),
('binu', 'binu1234', 'Doctor'),
('riji', 'riji1234', 'Doctor'),
('charls', 'charls1234', 'Doctor'),
('sujetha', 'sujetha1234', 'Doctor'),
('dawnmohan', 'dawn1234', 'Doctor'),
('amalkhosh', 'amalkhosh1234', 'Doctor'),
('sunilkumar', 'sunilkumar123', 'Doctor'),
('anoops', 'anoop1234', 'Doctor');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `pid` int(30) NOT NULL AUTO_INCREMENT,
  `opno` int(15) NOT NULL,
  `pname` varchar(30) NOT NULL,
  `pad` text NOT NULL,
  `pgen` varchar(30) NOT NULL,
  `pag` int(11) NOT NULL,
  `pctype` varchar(30) NOT NULL,
  `pjndate` date NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`pid`, `opno`, `pname`, `pad`, `pgen`, `pag`, `pctype`, `pjndate`) VALUES
(145, 1, 'anu', 'anu bhavan', 'Female', 24, 'APL', '2016-08-31'),
(155, 2, 'ann', 'ann bhavan', 'Female', 22, 'APL', '2016-10-14');

-- --------------------------------------------------------

--
-- Table structure for table `patientrecord`
--

CREATE TABLE IF NOT EXISTS `patientrecord` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(30) NOT NULL,
  `opno` int(15) NOT NULL,
  `tcode` int(15) NOT NULL,
  `amt` int(12) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `patientrecord`
--

INSERT INTO `patientrecord` (`rid`, `pid`, `opno`, `tcode`, `amt`, `date`, `status`) VALUES
(1, '', 1, 2, 34, '2016-08-15', 0),
(2, '', 1, 4, 23, '2016-08-15', 0),
(3, '142', 1, 1, 10, '2016-08-29', 1),
(4, '142', 1, 1, 10, '2016-08-30', 1),
(5, '142', 1, 1, 10, '2016-08-30', 1),
(6, '142', 1, 1, 10, '2016-08-30', 1),
(7, '142', 1, 2, 10, '2016-08-30', 1),
(8, '142', 1, 2, 10, '2016-08-30', 1),
(9, '142', 1, 2, 10, '2016-08-30', 1),
(10, '142', 1, 1, 10, '2016-08-30', 1),
(11, '142', 1, 2, 10, '2016-08-30', 1),
(12, '142', 1, 3, 10, '2016-08-30', 1),
(13, '142', 1, 2, 10, '2016-08-30', 1),
(14, '142', 1, 3, 10, '2016-08-30', 1),
(15, '142', 1, 2, 10, '2016-08-30', 1),
(16, '143', 2, 2, 10, '2016-08-30', 1),
(17, '143', 2, 2, 10, '2016-08-30', 1),
(18, '142', 1, 3, 10, '2016-08-30', 1),
(19, '142', 1, 2, 10, '2016-08-30', 1),
(20, '142', 1, 3, 10, '2016-08-30', 1),
(21, '142', 1, 2, 10, '2016-08-30', 1),
(22, '142', 1, 3, 10, '2016-08-30', 1),
(23, '143', 2, 2, 10, '2016-08-30', 1),
(24, '143', 2, 1, 10, '2016-08-30', 1),
(25, '142', 1, 2, 10, '2016-08-30', 1),
(26, '142', 1, 6, 15, '2016-08-31', 1),
(27, '144', 2, 1, 20, '2016-08-31', 1),
(28, '146', 2, 2, 10, '2016-08-31', 1),
(29, '146', 2, 11, 150, '2016-08-31', 1),
(30, '145', 1, 13, 50, '2016-08-31', 1),
(31, '145', 1, 12, 150, '2016-08-31', 1),
(32, '145', 1, 6, 30, '2016-09-01', 1),
(33, '145', 1, 11, 180, '2016-09-01', 1),
(34, '145', 1, 5, 20, '2016-09-01', 1),
(35, '146', 2, 3, 10, '2016-09-01', 1),
(36, '145', 1, 1, 20, '2016-09-04', 1),
(37, '145', 1, 7, 30, '2016-09-05', 1),
(38, '145', 1, 8, 30, '2016-09-05', 1),
(39, '146', 2, 14, 35, '2016-09-05', 1),
(40, '145', 1, 3, 20, '2016-09-11', 1),
(41, '145', 1, 4, 20, '2016-09-11', 1),
(42, '145', 1, 3, 20, '2016-09-11', 1),
(43, '145', 1, 5, 20, '2016-09-11', 1),
(44, '145', 1, 3, 20, '2016-09-11', 1),
(45, '145', 1, 4, 20, '2016-10-02', 1),
(46, '145', 1, 1, 20, '2016-10-02', 1),
(47, '145', 1, 12, 150, '2016-10-02', 1),
(48, '145', 1, 3, 20, '2016-10-02', 1),
(49, '145', 1, 10, 40, '2016-10-03', 1),
(50, '145', 1, 7, 30, '2016-10-03', 1),
(51, '145', 1, 10, 40, '2016-10-03', 1),
(52, '145', 1, 2, 20, '2016-10-03', 1),
(53, '145', 1, 2, 20, '2016-10-04', 1),
(54, '145', 1, 12, 150, '2016-10-20', 1),
(55, '145', 1, 4, 20, '2016-10-23', 1),
(56, '155', 2, 11, 180, '2016-10-23', 1),
(57, '145', 1, 1, 20, '2016-10-24', 1),
(58, '145', 1, 1, 20, '2016-10-24', 1),
(59, '145', 1, 7, 30, '2016-10-24', 1),
(60, '145', 1, 15, 40, '2016-10-24', 1),
(61, '145', 1, 12, 150, '2016-10-24', 1),
(62, '145', 1, 4, 20, '2016-10-25', 1),
(63, '145', 1, 4, 20, '2016-10-25', 1),
(64, '145', 1, 14, 45, '2016-10-25', 1),
(65, '145', 1, 7, 30, '2016-10-26', 1),
(66, '145', 1, 8, 30, '2016-10-29', 1),
(67, '145', 1, 6, 30, '2016-10-29', 1),
(68, '155', 2, 12, 150, '2016-10-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `scode` int(50) NOT NULL AUTO_INCREMENT,
  `sname` varchar(50) NOT NULL,
  `sge` varchar(11) NOT NULL,
  `sdes` varchar(50) NOT NULL,
  `squa` varchar(50) NOT NULL,
  `sdep` varchar(50) NOT NULL,
  `spht` varchar(50) NOT NULL,
  `scont` varchar(50) DEFAULT NULL,
  `seml` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`scode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`scode`, `sname`, `sge`, `sdes`, `squa`, `sdep`, `spht`, `scont`, `seml`) VALUES
(18, 'Sundaresan', 'Male', 'Clerk', 'Degree', '9', 'photo/female.jpg', '', ''),
(17, 'Geetha K', 'Female', ' Head Nurse', 'General Nursing', '9', 'photo/female.jpg', '', ''),
(16, 'Susheela T', 'Female', ' Head Nurse', 'General Nursing', '9', 'photo/female.jpg', '', ''),
(1, 'Dr.Sunilkumar', 'Male', 'Doctor', 'MBBS,DCH', '8', 'photo/female.jpg', '', ''),
(2, 'Dr.Anoop.S', 'Male', 'Doctor', 'MBBS,DCH', '8', 'photo/anoop.jpg', '', ''),
(3, 'Dr.Amalkhosh', 'Male', 'Doctor', 'MBBS,MD', '8', 'photo/amalkhosh.jpg', '', ''),
(4, 'Dr.Dawn Mohan', 'Male', 'Doctor', 'MBBS D-ORTHO', '4', 'photo/dawn.jpg', '', ''),
(5, 'Dr.Jane', 'Female', 'Doctor', 'MBBS,MS', '3', 'photo/jane.jpg', '', ''),
(6, 'Dr.Sujetha', 'Female', 'Doctor', 'MBBS,MS', '3', 'photo/female.jpg', '', ''),
(7, 'Dr.Charls', 'Male', 'Doctor', 'MBBS(NRHM)', '6', 'photo/female.jpg', '', ''),
(8, 'Dr.Riji', 'Female', 'Doctor', 'MBBS,MD', '5', 'photo/female.jpg', '', ''),
(9, 'Dr.Binu', 'Male', 'Doctor', 'BDS,MDS', '7', 'photo/binu.jpg', '', ''),
(10, 'Dr.Manoj ', 'Male', 'Doctor', 'MBBS,DCH,DNB,PGDDN,PGDAP', '1', 'photo/female.jpg', '', ''),
(11, 'Dr.Athira', 'Female', 'Doctor', 'MBBS', '6', 'photo/female.jpg', '', ''),
(12, 'Dr.Shinu', 'Female', 'Doctor', 'MBBS(CMO)', '6', 'photo/female.jpg', '', ''),
(13, 'Dr.Jayadev', 'Male', 'Doctor', 'MBBS(CMO)', '6', 'photo/female.jpg', '', ''),
(14, 'Dr.Vrinda S Raj', 'Female', 'Doctor', 'MBBS,MS', '2', 'photo/v.jpg', '', ''),
(15, 'Sobha K', 'Female', ' Head Nurse', 'General Nursing', '9', 'photo/female.jpg', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

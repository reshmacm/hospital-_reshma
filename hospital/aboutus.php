<?php
ob_start();
session_start();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
         <p style="font-family:'Times New Roman', Times, serif;font-size:16px;"><b style="font:'Times New Roman', Times, serif;font-weight:bolder;color:rgb(35, 33, 69);" >
          <h2><span>About to</span> Hospital</h2></b>
          <div class="clr"></div>
          <p style="font-size:18px;font-family:'Times New Roman', Times, serif;line-height:27px;text-align:justify">
          Sasthamcotta Taluk Hospital is set up either by upgrading the existing Community Health Centre as and when the hobli in which CHC is notified as a taluk headquarters by the state government.  They are referred to as Sub-district (sub-divisional) hospital for the purpose of standardisation under the revised guidelines of Indian Public Health Standards (benchmarks for the quality expected from various components of public health care organisations).  Taluk Hospitals are below the district and above the block level (hobli level) hospitals (CHCs).<br />
<p style="font-size:20px;font-family:'Times New Roman', Times, serif;line-height:27px;">
Objectives of setting up Sasthamcotta Taluk Hospitals are:<br /><p style="font-size:18px;font-family:'Times New Roman', Times, serif;line-height:27px;">
1.To provide comprehensive secondary health care (specialist and referral services) to the community through the Sub-district Hospital.<br />
2.To achieve and maintain an acceptable standard of quality of care.<br />
3.To make the services more responsive and sensitive to the needs of the people of the taluk.<br />
 <p style="font-family:'Times New Roman', Times, serif;font-size:16px;"><b style="font:'Times New Roman', Times, serif;font-weight:bolder;color:rgb(35, 33, 69);" >
<h2><span>Our</span> Mission</h2></b>
<p style="font-size:18px;font-family:'Times New Roman', Times, serif;line-height:27px;">To improve the health and healing of the people and communities we serve.
 <p style="font-family:'Times New Roman', Times, serif;font-size:16px;"><b style="font:'Times New Roman', Times, serif;font-weight:bolder;color:rgb(35, 33, 69);" >
 <h2><span>Our</span> Vision</h2></b>
<p style="font-size:18px;font-family:'Times New Roman', Times, serif;line-height:27px;">To be nationally respected for exellence in patient care and most trusted for personalized coordinated care. 

          </div>
          </div>
          </div>
          </div>
</body>
</html>